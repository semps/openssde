"""
    MIT

    OpenSSDE - reads CT-dicom files to estimate patient size and extract information for choosing SSDE conversion factor
    Copyright (c) 2016, 2017 Josef Lundman & Morgan Nyberg

    This file is part of OpenSSDE.
"""
import numpy as np
import math
from scipy import ndimage
from skimage import morphology
from skimage.transform import hough_line, radon
from scipy.ndimage.measurements import center_of_mass
import matplotlib.pyplot as plt


#@profile
def segment_axial(image_matrix, visual=False, log=None):
    """ Takes a 3D matrix containing the reconstructed CT volume and extract the patient from that volume

    :param image_matrix: 3D image matrix with the reconstructed volume
    :param visual: Boolean determining if plots should be shown or not
    :param log: Handle for logging
    :return:
    """
    if log is not None:
        log.debug('Segmenting axial images')
    mask_success = False

    # Create an image mask covering all pixels with HU larger than -500.
    image_mask = np.zeros(image_matrix.shape)
    image_mask[image_matrix > -500] = 1

    # Remove tha table by eroding and dilating the image mask
    if image_mask.shape[2] > 2:
        image_mask = morphology.binary_erosion(image_mask, morphology.cube(3))
    else:
        for i in range(image_mask.shape[2]):
            image_mask[:, :, i] = morphology.binary_erosion(image_mask[:, :, i], morphology.disk(3))

    for i in range(image_mask.shape[2]):
        image_mask[:, :, i] = ndimage.morphology.binary_fill_holes(image_mask[:, :, i])

    # Select the object going through the centre of the image matrix as this will most likely be the patient
    image_mask, nb_labels = ndimage.label(image_mask)
    central_position = [int(np.floor(np.divide(float(image_mask.shape[0]), 2.0))),
                        int(np.floor(np.divide(float(image_mask.shape[1]), 2.0))),
                        int(np.floor(np.divide(float(image_mask.shape[2]), 2.0)))]
    central_blob = np.max(image_mask[(central_position[0]-2):(central_position[0]+2),
                          (central_position[1]-2):(central_position[1]+2),
                          central_position[2]])
    image_mask[image_mask != central_blob] = 0
    image_mask[image_mask == central_blob] = 1
    if np.sum(image_mask) > 0:
        if log is not None:
            log.debug('Axial images segmented successfully')
        mask_success = True
    # ------------------------------------------------------------------------

    image_mask = morphology.binary_dilation(image_mask, morphology.cube(3))

    if log is not None:
        log.debug('Checking if patient is likely clipped')
    # Check if it's likely that the patient was clipped
    patient_clipped = np.sum([np.sum(image_mask[:, 0, :]), np.sum(image_mask[:, (image_mask.shape[1]-1)]),
                              np.sum(image_mask[0, :, :]), np.sum(image_mask[(image_mask.shape[0]-1), :, :])]) > 0
    # if visual:
    #     plt.imshow(image_mask, cmap='bone', interpolation='nearest')
    #     plt.show()

    if visual:
        # Construct a contour around the mask
        edge_image_mask = ndimage.morphology.binary_dilation(image_mask, iterations=1) - image_mask

        # Mask the image with the edge of the mask
        edge_masked_image = image_matrix.copy()
        edge_masked_image[edge_image_mask] = 1200  # Colour the edge with high HU
        plt.imshow(edge_masked_image, cmap='bone')

        masked_image = image_matrix.copy()
        masked_image[~image_mask] = np.min(image_matrix)
        plt.imshow(masked_image, cmap='bone')
        return mask_success, image_mask, patient_clipped, edge_image_mask
    else:
        return mask_success, image_mask, patient_clipped, None


#@profile
def segment_localizer(image_matrix, window_range=range(-1000, 100), manufacturer_specific_settings=None, visual=False,
                      log=None):
    """Find the body contour in a localizer CT image.

    Find the body contour, and create a mask covering the body, in a localizer CT image by a sliding threshold
    algorithm.

    The sliding threshold algorithm threshold the pixels within a defined range of pixel values.
    The speed of which the ratio of threshold pixels compared to the total number of pixels is a measure of
    the number of pixels with similar pixel values in the image.
    Homogeneous areas, such as the surrounding air and the table top, consists of large number of pixels
    with similar pixel values regardless of how different manufacturers have chosen to implement their routines.
    The peaks in the derivative ratio of thresholded pixels indicates pixel values of air and the table top.

    Manufacturer specific ranges and offsets to the peaks are set accordingly.

    The crude thresholded mask is dilated and eroded to close small gaps before the blobs are filled.
    Blobs smaller than an arbitrary mask size limit are removed.

    A contour around the mask is created.

    The contour and mask is stored in the image object.

    :param image_matrix: 3D image matrix with the reconstructed volume
    :param window_range: The manufacturer specific window range
    :param manufacturer_specific_settings: Manufacturer specific settings for the segmentation, e.g. convolution offset
    :param visual: Boolean determining if plots should be shown or not
    :param log: Handle for logging
    :return:
    """
    if log is not None:
        log.debug('Segmenting localizer')
    # from skimage.feature import canny
    image_size = image_matrix.shape[0] * image_matrix.shape[1]

    # Mask away everything filling less than 5% of the image (5% chosen to allow for children and phantoms
    mask_size_limit = 0.05 * image_size

    hu_min = np.min(image_matrix)
    # hu_max = np.max(image_matrix)

    # Create an array to store the resulting list of ratios of the number of thresholded pixels over the total number of
    # pixels
    ratio_list = np.empty_like(window_range, dtype=float)
    delta_window_range_step = 1.0
    for i in range(len(window_range)):
        window_threshold = window_range[i]
        image_mask = np.ones_like(image_matrix)
        image_mask[image_matrix < window_threshold] = 0
        ratio_list[i] = np.sum(image_mask) / image_size
        if visual:
            image_mask[0, 0] = 0

    # Calculate the derivative of the ratio curve to detect steep slopes as peaks
    # Use mode 'same' to avoid off-by-one array
    conv_ratio = np.convolve(ratio_list, [1, -1], mode='same') / delta_window_range_step

    # Manufacturer specific threshold
    if manufacturer_specific_settings is not None:
        if 'ConvolvingOffset' in manufacturer_specific_settings.keys():
            conv_offset = manufacturer_specific_settings['ConvolvingOffset']
        else:
            conv_offset = 10
        if 'WindowRangeIndexOffset' in manufacturer_specific_settings.keys():
            window_range_offset = manufacturer_specific_settings['WindowRangeIndexOffset']
        else:
            window_range_offset = 0
        if log is not None:
            log.debug('Segmenting with manufacturer specific settings')
    else:
        conv_offset = 10
        window_range_offset = 0
        if log is not None:
            log.debug('Segmenting with default settings')

    window_range_index = conv_ratio[conv_offset:].argmin() + conv_offset + window_range_offset
    if visual:
        plt.plot(window_range_index)
    window_range_choice = window_range[window_range_index]
    final_ratio_choice = ratio_list[window_range_index]

    # Make the mask at from the selected threshold
    image_mask = np.ones_like(image_matrix)
    image_mask[image_matrix < window_range_choice] = 0
    if visual:
        plt.imshow(image_mask, cmap='bone', interpolation='nearest')
        plt.show()

    # Clean small stuff by eroding and dilating
    image_mask = ndimage.morphology.binary_dilation(ndimage.binary_erosion(image_mask, iterations=2), iterations=2)

    # Preclean larger blobs. skip the preclean when the chosen ratio is low as this indicate phantom instead of patient
    if final_ratio_choice > 0.25:
        label_objects, nb_labels = ndimage.label(image_mask)
        sizes = np.bincount(label_objects.ravel())
        mask_sizes = sizes > mask_size_limit
        mask_sizes[0] = 0
        image_mask = mask_sizes[label_objects]

    # Close smaller gaps by dilate and erode
    image_mask = close_small_gaps(image_mask)

    # Remove objects smaller than the mask size limit
    # Label the connected blobs in the binary image and then measure their size and erase the ones smaller than the
    # chosen size limit.
    # Lastly, check whether there remains a mask or completely empty image mask
    label_objects, nb_labels = ndimage.label(image_mask)
    sizes = np.bincount(label_objects.ravel())
    mask_sizes = sizes > mask_size_limit
    mask_sizes[0] = 0
    image_mask = mask_sizes[label_objects]

    # Check if there is any mask
    mask_success = not(np.sum(image_mask) == 0)
    if not mask_success:
        if log is not None:
            log.error('Could not mask the localizer image')
        print('Could not mask the localizer image')
        raise RuntimeError('Could not mask the localizer image')

    if log is not None:
        log.debug('Localizer image masked successfully')
    if visual:
        # Construct a contour around the mask
        edge_image_mask = ndimage.morphology.binary_dilation(image_mask, iterations=1) - image_mask

        # Mask the image with the edge of the mask
        edge_masked_image = image_matrix.copy()
        edge_masked_image[edge_image_mask] = 1200  # Colour the edge with high HU
        plt.imshow(edge_masked_image, cmap='bone')

        masked_image = image_matrix.copy()
        masked_image[~image_mask] = hu_min
        plt.imshow(masked_image, cmap='bone')
        return [mask_success, image_mask, edge_image_mask]
    else:
        return [mask_success, image_mask, None]


#@profile
def close_small_gaps(image_mask, verbose=False, log=None):
    """ close small gaps and kinks by dilate and erode

    Clean up a binary image mask by closing small gaps and kinks using dilation follow by same amount of erosion.

    This might connect blobs in an unwanted fashion so this process is performed with two strengths.
    Uses the more gentle process if the more aggressive dilation and erosion process connects separate structure.
    The algorithm decides by performing a cleaning of smaller blobs and compares the resulting masks.
    If unwanted connections has arised then the difference between the image mask sizes will be considerate.

    Usage:
        filled_image = close_small_gaps(bildmask)

    Uses:
        numpy->np.sum
        scipy->ndimage.binary_fill_holes
        scipy->ndimage.morphology.binary_dilation
        scipy->ndimage.binary_erosion

    Used by:
        SSDEcalculations.py->segmenteraScout

    :param image_mask: binary image mask to close small gaps and kinks in
    :param verbose: boolean specifying whether stuff should be printed
    :param log: Handle for logging
    :return: image mask with small gaps and kinks closed
    """
    if log is not None:
        log.debug('Closing small gaps in the mask')
    # close small stuff by dilate and erode
    # try this with two strengths but avoid to connect 'table blobs'
    image_size = image_mask.shape[0] * image_mask.shape[1]

    dilation_size = 1
    eroded_fill_image1 = ndimage.binary_erosion(
        ndimage.binary_fill_holes(ndimage.morphology.binary_dilation(image_mask, iterations=dilation_size)),
        iterations=dilation_size
    )

    dilation_size = 3
    eroded_fill_image2 = ndimage.binary_erosion(
        ndimage.binary_fill_holes(ndimage.morphology.binary_dilation(image_mask, iterations=dilation_size)),
        iterations=dilation_size
    )

    fill_image_diff_sum = np.sum(eroded_fill_image2 - eroded_fill_image1)
    if verbose:
        print('diff = ' + str(fill_image_diff_sum) + ' ratio: ' + str(fill_image_diff_sum/image_size))

    # if the mask suddenly became much larger it probably connected the patient mask with a 'table blob'
    if fill_image_diff_sum <= 0.01*image_size:
        filled_image = eroded_fill_image2
        if verbose:
            print('dil2')
    else:
        filled_image = eroded_fill_image1
        if verbose:
            print('dil1')

    if log is not None:
        log.debug('Small gaps closed')

    return filled_image


#@profile
def calculate_equivalent_diameter(image_matrix=None, image_mask=None, dicom_headers=None,
                                  perform_hough_or_radon_transform=0, visual=False, log=None):
    """ Calculate the equivalent diameter and circumference for axial images.

    This function is implemented for axial images.
    It calculates:
     - the area equivalent diameter and circumference of the patient,
     - the mean and median HU-value inside the masked body
     - the WED according to AAPM Report 220 §2.1
     - the lateral (LAT) and posteriaterio (AP) measure of the patient, and AP+LAT
     - the elliptical, effective diameter according to AAPM Report 204

    The function also illustrates the WED as a diameter drawn through the body slice center of mass
    as well as the area equivalent diameter as a circle around the body slice center of mass.

    Currently, the function also performs a Radon transformation and finds, in the resulting sinogram,
    the largest and smallest patient width at their respective rotational degrees.
    This could be used to discern whether the patient is positioned flat at the bed or rotated.

    The patient size values are stored in the image object.

    Usage:
        calculateEquivDiameter(self.currentImage)

    Uses:


    Used by:


    :param perform_hough_or_radon_transform: Specify whether Hough or radon transform should be performed. 0 = No transform, 1 = Hough transform, 2 = Radon transform
    :param dicom_headers:
    :param image_matrix:
    :param image_mask:
    :param log: Handle for logging
    :return:
    """
    if log is not None:
        log.debug('Calculating equivalent diameter for axials')
    high_density_voxels = np.sum((image_matrix*image_mask) > 2900)
    high_density_voxels_percent = float(high_density_voxels) / float(np.sum(image_mask)) * 100.0
    if visual:
        from skimage.draw import circle_perimeter

    # Loop through the image matrix and calculate the equivalent diameter
    for i in range(image_matrix.shape[2]):
        dicom_headers[i].HighDensityVoxelsSeries = high_density_voxels
        dicom_headers[i].HighDensityVoxelsSeriesPercent = high_density_voxels_percent
        pixel_spacing = dicom_headers[i][0x28, 0x30].value

        image = image_matrix[:, :, i].copy()
        mask = image_mask[:, :, i]

        # Count the number of pixels in the patient mask for the current slice, corresponds to the area (in pixels)
        mask_area_px = np.sum(image_mask[:, :, i])

        # A = PI * d^2 /4  -> d^2 = 4A/PI -> d = 2*SQRT(A/PI)
        # O = PI * d -> PI * 2 * SQRT(A)/SQRT(PI) -> 2*SQRT(A*PI)
        mask_eqv_diameter_px = 2.0*math.sqrt(mask_area_px/math.pi)
        mask_eqv_diameter_cm = mask_eqv_diameter_px*pixel_spacing[0]/10.0
        mask_eqv_circumference_px = 2.0*math.sqrt(mask_area_px*math.pi)
        mask_eqv_circumference_cm = mask_eqv_circumference_px*pixel_spacing[0]/10.0

        # Store the calculated equivalen area and diameter in the dicom_header object
        dicom_headers[i].BodyAreaPx = mask_area_px
        dicom_headers[i].EquivalentAreaDiameter_px = mask_eqv_diameter_px
        dicom_headers[i].EquivalentAreaDiameter_cm = mask_eqv_diameter_cm
        dicom_headers[i].EquivalentAreaCircumference_cm = mask_eqv_circumference_cm

        # Calculate sum, mean and median of HU in the patient
        sum_hu = np.sum(image[mask])
        mean_hu = np.mean(image[mask])
        median_hu = np.median(image[mask])

        # Aw = 1/1000 * AvgCT*Aroi + Aroi  from AAPM Report 220 §2.1
        # Aw = sum(CT(x,y))/1000 * Apixel + sum(Apixel)  from AAPM Report 220 §2.1
        # -> = sum(CT(x,y))/1000 * Apixel + Npixel*Apixel
        # -> = (sum(CT(x,y))/1000 + Npixel) * Apixel   # Apixel = area of pixel = pixelspacing[0]*pixelspacing[1]
        Aw_px = mean_hu/1000.0*mask_area_px + mask_area_px
        Aw_cm = Aw_px*pixel_spacing[0]*pixel_spacing[1]/100.0
        wed_px = 2.0*math.sqrt(Aw_px/math.pi)
        wed_cm = wed_px*pixel_spacing[0]/10.0

        # Store calculated values in the dicom_header object
        dicom_headers[i].MeanHU = mean_hu
        dicom_headers[i].MedianHU = median_hu
        dicom_headers[i].WED_px = wed_px
        dicom_headers[i].WED_cm = wed_cm

        # Get AP & LAT from the image mask
        lat_px = np.count_nonzero(np.sum(mask, axis=0))
        lat_cm = lat_px*pixel_spacing[0]/10.0
        ap_px = np.count_nonzero(np.sum(mask, axis=1))
        ap_cm = ap_px*pixel_spacing[1]/10.0

        # Store calculated values in the dicom header object
        dicom_headers[i].LAT_cm = lat_cm
        dicom_headers[i].AP_cm = ap_cm

        # Store the elliptical efficient diameter (EED) as suggested in AAPM Report 204
        # Calculated from the AP and LAT measurement
        dicom_headers[i].EED_px = math.sqrt(ap_px*lat_px)
        dicom_headers[i].EED_cm = dicom_headers[i].EED_px*pixel_spacing[0]/10.0

        # Calculate the centre of mass for the patient mask
        mask_centre_of_mass = ndimage.measurements.center_of_mass(mask)
        dicom_headers[i].GeometricalCentre = (round(mask_centre_of_mass[0], 1), round(mask_centre_of_mass[1], 1))

        if visual:
            mass_centre_row = int(round(dicom_headers[i].GeometricalCentre[0]))
            mass_centre_col = int(round(dicom_headers[i].GeometricalCentre[1]))
            mask_circle_perimeter = circle_perimeter(mass_centre_row,
                                                     mass_centre_col,
                                                     int(round(0.5*mask_eqv_diameter_px)))
            image[~mask] = np.min(image)
            image[mask_circle_perimeter] = np.max(image)

            # Add line corresponding to the WED through the centre of mass
            line_start = mass_centre_col - int(round(0.5*dicom_headers[i].WED_px))
            line_stop = mass_centre_col + int(round(0.5 * dicom_headers[i].WED_px))
            image[mass_centre_row-1:mass_centre_row+1, line_start:line_stop] = np.max(image)
            plt.title('Circle: area equivalent diameter. Line: WED')
            plt.imshow(image, cmap='bone', interpolation='nearest')

        if perform_hough_or_radon_transform > 0:
            if perform_hough_or_radon_transform > 2:
                if log is not None:
                    log.error('Invalid transform selected. No transform is performed')
                print('The selected transform is out of bounds. No transform will be performed.')
                pass
            if perform_hough_or_radon_transform == 1:
                # Perform the Hough transform to find the largest and smallest distances over the patient mask.
                h, angles, d = hough_line(mask, np.linspace(0, np.pi - np.pi/180, 180))

                # Calculate the number of non-zero elements for each Hough transform line
                non_zero = [np.count_nonzero(h[:, i]) for i in range(0, 180)]

                # Find the maximum and minimum distance through the patient and the angles at which these occur
                sinogram_max_px = max(non_zero)
                sinogram_max_degree = angles[non_zero.index(sinogram_max_px)]*180/np.pi
                sinogram_min_px = min(non_zero)
                sinogram_min_degree = angles[non_zero.index(sinogram_min_px)]*180/np.pi

            if perform_hough_or_radon_transform == 2:
                mask_radon = radon(mask)
                sinogram_max_px = 0
                sinogram_max_degree = 0
                sinogram_min_px = np.size(mask_radon, axis=0)
                sinogram_min_degree = 0
                for rot_degree in range(0,180):
                    non_zero_px = np.count_nonzero(mask_radon[:, rot_degree])
                    if non_zero_px > sinogram_max_px:
                        sinogram_max_px = non_zero_px
                        sinogram_max_degree = rot_degree
                    if non_zero_px < sinogram_min_px:
                        sinogram_min_px = non_zero_px
                        sinogram_min_degree = rot_degree

            sinogram_max_cm = sinogram_max_px * pixel_spacing[0] / 10.0
            sinogram_min_cm = sinogram_min_px * pixel_spacing[0] / 10.0
            dicom_headers[i].SinogramLATcm = sinogram_max_cm
            dicom_headers[i].SinogramLATDegree = sinogram_max_degree
            dicom_headers[i].SinogramAPcm = sinogram_min_cm
            dicom_headers[i].SinogramAPDegree = sinogram_min_degree
            dicom_headers[i].SinogramEDcm = math.sqrt(sinogram_max_px*sinogram_min_px)*pixel_spacing[0]/10.0

    if log is not None:
        log.debug('Finished equivalent diameter calculations for axial images')

    return dicom_headers

#@profile
def estimate_scout(image_matrix, image_mask, dicom_header, manufacturer_specific_settings=None,
                   visual=False, verbose=False, log=None):
    """ Calculate the patient size for localizer images.

    This function is implemented for localizer images. It calculates:
    - The lateral (LAT) or ateroposterior (AP) measure of the patient
    - The localizer-WED as a variant of Menke 2005 10.1148/radiol.2362041327

    The patient size values are stored in the dicom header object.

    Usage:
    estimate_scout(image_matrix, dicom_header, visual, report)

    Uses:


    Used by:


    :param manufacturer_specific_settings:
    :param verbose:
    :param image_matrix:
    :param image_mask:
    :param dicom_header:
    :param visual:
    :param log: Handle for logging
    :return:
    """
    pixel_spacing = [float(i) for i in dicom_header[0x28, 0x30].value]
    rows, columns = image_mask.shape
    orientation = [float(i) for i in dicom_header.ImageOrientationPatient]
    L_results = None
    # Check the patient orientation to know whether to patient size should be calculated in rows or columns
    if abs(dicom_header[0x200037].value[2]) == 1.0:
        # This means the x-axis of the image is in the z-direction of the patient. The patient size is calculated by
        # the number of non-zero pixels per column in the image mask
        midcol = image_mask[:, round(0.5*columns)]
        L_i = np.nonzero(midcol)
        L_px = np.count_nonzero(midcol)
        L_cm = L_px * pixel_spacing[1] / 10.0
        L_pos = dicom_header.ImagePositionPatient[2] - round(0.5 * columns) * pixel_spacing[1]
        dicom_header.ImagePosition = L_pos

        xrow, xcol, yrow, ycol, zrow, zcol = dicom_header[0x20, 0x37].value
        if (1.0 - abs(xrow)) < 0.01:
            dicom_header.AP_cm = L_cm
        else:
            dicom_header.LATcm = L_cm

        # Store calculated values in the dicom header object
        # Loop through all the rows and make a list with position and size, maybe store the mid-position also
        L_results = np.zeros([columns, 2])
        for col in range(columns):
            L_px = np.count_nonzero(image_mask[:, col])
            L_pos = dicom_header.ImagePosition + col * pixel_spacing[1] / 10.0
            # For each row, calculate line length and store with its z-position. If row is empty, skip L_cm calculation
            if L_px:
                L_cm = L_px * pixel_spacing[1] / 10.0
                L_results[col, 0] = L_pos
                L_results[col, 1] = L_cm
            else:
                L_results[col, 0] = L_pos
                L_results[col, 1] = 0.0

    else:
        # If the x-axis of the localizer is not in the z-direction of the patient, the y-axis is. Therefor the patient
        # size should be calculated by the number of non-zero pixels per row in the image mask
        midrow = image_mask[round(0.5*rows), :]
        L_i = np.nonzero(midrow)
        L_px = np.count_nonzero(midrow)
        L_cm = L_px*pixel_spacing[0]/10.0
        L_pos = dicom_header.ImagePositionPatient[2] - round(0.5*rows)*pixel_spacing[0]
        dicom_header.ImagePosition = L_pos

        xrow, xcol, yrow, ycol, zrow, zcol = dicom_header[0x20, 0x37].value
        if (1.0 - abs(xcol)) < 0.01:
            dicom_header.AP_cm = L_cm
        else:
            dicom_header.LATcm = L_cm

        # Store calculated values in the dicom header object
        # Loop through all the rows and make a list with position and size, maybe store the mid-position also
        L_results = np.zeros([rows, 2])
        for row in range(rows):
            L_px = np.count_nonzero(image_mask[row, :])
            L_pos = dicom_header.ImagePosition + row*pixel_spacing[0]/10.0
            # For each row, calculate line length and store with its z-position. If row is empty, skip L_cm calculation
            if L_px:
                L_cm = L_px * pixel_spacing[0]/10.0
                L_results[row, 0] = L_pos
                L_results[row, 1] = L_cm
            else:
                L_results[row, 0] = L_pos
                L_results[row, 1] = 0.0

    # Store the result in the dicom header object
    dicom_header.LocalizerResults = L_results

    # Calculate the water equivalent diameter (WED) using the method presented by Menke (2005)
    image = image_matrix.copy()
    image[~image_mask] = 0
    A_roi = L_i[0][-1] - L_i[0][0] + 1  # The difference between the first and last pixel plus 1 = number of pixels
    L_avg = np.average(image[round(0.5*rows), L_i])  # The mean of the elements covered by the line, AAPM Report 220
    dicom_header.WEDpx = (0.01*L_avg + 1.0)*A_roi
    dicom_header.WEDcm = dicom_header.WEDpx*pixel_spacing[0]/10.0

    if log is not None:
        log.debug('WED calculated for localizer')

    # Estimate the WED from the localizer image
    dicom_header = menke2005(image_matrix, image_mask, dicom_header, manufacturer_specific_settings, verbose)

    return dicom_header


def menke2005(image_matrix, image_mask, dicom_header, manufacturer_specific_settings=None, verbose=False, log=None):
    """

    :param manufacturer_specific_settings:
    :param verbose:
    :param image_matrix:
    :param image_mask:
    :param dicom_header:
    :param log: Handle for logging
    :return:
    """
    if log is not None:
        log.debug('Starting the Menke calculations for each row')
    if verbose:
        print('Starting the Menke calculations for each row')

    pixel_size = dicom_header[0x28, 0x30].value[0]

    image_matrix[~image_mask] = 0

    rows, columns = np.shape(image_mask)

    # Set standard (Siemens) pixel values for air and water. Use decimal zero to avoid DuckTyping to integer
    pxv_air = -53.0
    pxv_water = 43.0
    if manufacturer_specific_settings is not None:
        if hasattr(manufacturer_specific_settings[0], 'LocalizerPixelValueWater'):
            pxv_water = float(manufacturer_specific_settings[0]['LocalizerPixelValueWater'])
        if hasattr(manufacturer_specific_settings[0], 'LocalizerPixelValueAir'):
            pxv_air = float(manufacturer_specific_settings[0]['LocalizerPixelValueAir'])
        if log is not None:
            log.debug('Using manufacturer specific pixel values')
    else:
        if log is not None:
            log.debug('No manufacturer specific settings, using default pixel values')

    pxv_scale = pxv_water - pxv_air

    menke_results = np.zeros([rows, 2])
    for row in range(rows):
        nonzero_i = np.nonzero(image_mask[row, :])
        nonzero_px = np.count_nonzero(image_mask[row, :])
        row_pos = dicom_header.ImagePositionPatient[2] - row*pixel_size

        if nonzero_px:
            row_avg = np.average(image_matrix[row, nonzero_i])
            A_roi = nonzero_i[0][-1] - nonzero_i[0][0] + 1 # The area of the ROI is the pixels of the line
            menke_wed_px = (row_avg - pxv_air)/pxv_scale * A_roi
            menke_wed_cm = menke_wed_px*pixel_size/10.0
            menke_results[row, 0] = row_pos
            menke_results[row, 1] = menke_wed_cm
        else:
            menke_results[row, 0] = row_pos
            menke_results[row, 1] = 0.0

    if log is not None:
        log.debug('Finished the Menke calculations')
    if verbose:
        print('Finished the Menke calculations for each row in the localizer')

    dicom_header.MenkeResults = menke_results

    return dicom_header


def calculate_patient_geometrical_offset(mask_slice, dicom_header, verbose=False, log=None):
    """Calculate the patient geometrical offset

    :param mask_slice:
    :param dicom_header:
    :param verbose:
    :param log: Handle for logging
    :return:
    """
    if log is not None:
        log.debug('Calculating patient geometrical offset')

    if hasattr(dicom_header, 'ImagePositionPatient'):

        # Calculate patient offset
        mask_centre_of_mass = center_of_mass(mask_slice)
        geometrical_centre = (round(mask_centre_of_mass[0], 1), round(mask_centre_of_mass[1], 1))
        image_position = (float(dicom_header.ImagePositionPatient[0]), float(dicom_header.ImagePositionPatient[1]))
        if hasattr(dicom_header, 'Manufacturer'):
            if dicom_header.Manufacturer == 'SIEMENS':
                image_position = (image_position[0], image_position[1] + float(dicom_header.TableHeight))
            elif dicom_header.Manufacturer == 'Philips':
                image_position = (image_position[0], image_position[1] - float(dicom_header.TableHeight))

        patient_offset = (
            float(image_position[0]) + (geometrical_centre[1] * float(dicom_header.PixelSpacing[0])),
            float(image_position[1]) + (geometrical_centre[0] * float(dicom_header.PixelSpacing[1]))
        )
    else:
        if hasattr(
                dicom_header,
                'DataCollectionCenterPatient'
        ) and hasattr(
            dicom_header,
            'ReconstructionTargetCenterPatient'
        ):
            diff_table_x_mm = float(dicom_header.ReconstructionTargetCenterPatient[0]) -\
                              float(dicom_header.DataCollectionCenterPatient[0])

            diff_table_y_mm = float(dicom_header.ReconstructionTargetCenterPatient[1]) -\
                              float(dicom_header.DataCollectionCenterPatient[1])
        elif dicom_header.Manufacturer in ['GE MEDICAL SYSTEMS'] and dicom_header[0x431031]:
            diff_table_x_mm = float(dicom_header[0x431031].value[0])
            diff_table_y_mm = float(dicom_header[0x431031].value[1])
        elif dicom_header.Manufacturer in ['TOSHIBA'] and dicom_header[0x70051007]:
            tmp = [float(i) for i in dicom_header[0x70051007].value.decaode('utf-8').split('\\')]
            diff_table_x_mm = (tmp[0]-mask_slice.shape[0]/2.0)*float(dicom_header.PixelSpacing[0])
            diff_table_y_mm = (tmp[1] - mask_slice.shape[1] / 2.0) * float(dicom_header.PixelSpacing[1])
        else:
            if log is not None:
                log.error('Object does not have required DICOM header tags')
            if verbose:
                print('Object does not have all required DICOM header tags')
            raise IOError('The DICOM header does not contain all required tags')

        # Calculate patient offset
        x_size_px, y_size_px = mask_slice.shape
        image_center_x = x_size_px / 2.0
        image_center_y = y_size_px / 2.0

        mask_centre_of_mass = center_of_mass(mask_slice)
        geometrical_centre = (round(mask_centre_of_mass[0], 1), round(mask_centre_of_mass[1], 1))

        diff_geom_x_center_mm = (geometrical_centre[0] - image_center_x) * float(dicom_header.PixelSpacing[0])
        diff_geom_y_center_mm = (geometrical_centre[1] - image_center_y) * float(dicom_header.PixelSpacing[1])

        patient_offset = (diff_table_x_mm - diff_geom_x_center_mm), (diff_table_y_mm - diff_geom_y_center_mm)

    return patient_offset
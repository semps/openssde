"""
    MIT

    OpenSSDE - reads CT-dicom files to estimate patient size and extract information for choosing SSDE conversion factor
    Copyright (c) 2016, 2017 Josef Lundman & Morgan Nyberg

    This file is part of OpenSSDE.
"""
import datetime
import os.path
from conversion_functions import *


def export_axial_result(dicom_headers, export2='database', output_file=None, database_connection_parameters=None, mask_success=False):
    """ Export the SSDE calculation results for axial images.

    :param dicom_header:
    :param export2:
    :param output_file:
    :param database_connection_parameters:
    :return:
    """
    if export2 == 'file':
        if output_file is None:
            print("No output file specified")
            return False
        else:
            datedata = datetime.date.today()
            datestr = datedata.isoformat()
            output_file = os.path.normcase(output_file)

        # Setup  the header list
        header_string = ["Study Instance UID"]
        header_string += ['Accession NUmber']
        header_string += ["Acquisition Number"]
        header_string += ["mask success"]
        header_string += ["Image type"]
        header_string += ["Manufacturer"]
        header_string += ["Model"]
        header_string += ["kVp"]
        header_string += ["mAs"]
        header_string += ["Slice Thickness"]
        header_string += ["CTDIvol"]
        header_string += ["Table Height"]
        header_string += ["Z position"]
        header_string += ["mean HU"]
        header_string += ["median HU"]
        header_string += ["WED (cm)"]  # water equivalent diameter
        header_string += ["CF 16 WED"]
        header_string += ["CF 32 WED"]
        header_string += ["LAT (cm)"]
        header_string += ["CF 16 LAT"]
        header_string += ["CF 32 LAT"]
        header_string += ["AP (cm)"]
        header_string += ["CF 16 AP"]
        header_string += ["CF 32 AP"]
        header_string += ["AP + LAT (cm)"]
        header_string += ["CF 16 AP + LAT"]
        header_string += ["CF 32 AP + LAT"]
        header_string += ["EED (cm)"]  # elliptical efficient diameter SQRT(AP*LAT)
        header_string += ["CF 16 EED"]
        header_string += ["CF 32 EED"]
        header_string += ["EAD (cm)"]  # equal area diameter
        header_string += ["Sinogram ED (cm)"]  # SQRT(AP*LAT)
        header_string += ["CF 16 sinogram ED"]
        header_string += ["CF 32 sinogram ED"]
        header_string += ["Sinogram LAT (cm)"]
        header_string += ["Sinogram LAT (degree)"]
        header_string += ["CF 16 sinogram LAT"]
        header_string += ["CF 32 sinogram LAT"]
        header_string += ["Sinogram AP (cm)"]
        header_string += ["Sinogram AP (degree)"]
        header_string += ["CF 16 sinogram AP"]
        header_string += ["CF 32 sinogram AP"]
        header_string += ["ImageType"]

    elif export2 == "database":
        # Choose action depending on the database engine
        if database_connection_parameters['engine'] == "postgresql":
            import psycopg2
            conn = psycopg2.connect(database=database_connection_parameters['database'],
                                    user=database_connection_parameters['user'],
                                    password=database_connection_parameters['password'],
                                    host=database_connection_parameters['host'],
                                    port=database_connection_parameters['port'])
        else:
            print("There is no packages to use for the specified database engine")
            return False

        n = 0
        for dicom_header in dicom_headers:
            if hasattr(dicom_header, 'AP_cm') and (dicom_header.AP_cm is not None) and \
                    hasattr(dicom_header, 'LAT_cm') and (dicom_header.LAT_cm is not None):
                APLAT = dicom_header.AP_cm + dicom_header.LAT_cm
            else:
                APLAT = None

            cur = conn.cursor()

            if n == 0:
                output_data0 = dict()

                if hasattr(dicom_header, 'SOPInstanceUID'):
                    output_data0['SOPInstanceUID'] = dicom_header.SOPInstanceUID
                else:
                    output_data0['SOPInstanceUID'] = None

                if hasattr(dicom_header, 'StudyInstanceUID'):
                    output_data0['StudyInstanceUID'] = dicom_header.StudyInstanceUID
                else:
                    output_data0['StudyInstanceUID'] = None

                if hasattr(dicom_header, 'SeriesInstanceUID'):
                    output_data0['SeriesInstanceUID'] = dicom_header.SeriesInstanceUID
                else:
                    output_data0['SeriesInstanceUID'] = None

                output_data0['MaskSuccess'] = mask_success

                if hasattr(dicom_header, 'ImageType'):
                    output_data0['ImageType'] = dicom_header.ImageType[2]
                else:
                    output_data0['ImageType'] = None

                if hasattr(dicom_header, 'Manufacturer'):
                    output_data0['Manufacturer'] = dicom_header.Manufacturer
                else:
                    output_data0['Manufacturer'] = None

                if hasattr(dicom_header, 'ManufacturerModelName'):
                    output_data0['Model'] = dicom_header.ManufacturerModelName
                else:
                    output_data0['Model'] = None

                if hasattr(dicom_header, 'KVP'):
                    output_data0['kVp'] = float(dicom_header.KVP)
                else:
                    output_data0['kVp'] = None

                if hasattr(dicom_header, 'TableHeight'):
                    output_data0['TableHeight'] = dicom_header.TableHeight
                else:
                    output_data0['TableHeight'] = None

                if hasattr(dicom_header, 'PatientClipped'):
                    if dicom_header.PatientClipped:
                        output_data0['PatientClipped'] = True
                    else:
                        output_data0['PatientClipped'] = False
                else:
                    output_data0['PatientClipped'] = None

                if hasattr(dicom_header, 'PatientOffset'):
                    if dicom_header.PatientOffset is not None:
                        output_data0['PatientGeometricalOffsetX'] = dicom_header.PatientOffset[0]
                        output_data0['PatientGeometricalOffsetY'] = dicom_header.PatientOffset[1]
                    else:
                        output_data0['PatientGeometricalOffsetX'] = None
                        output_data0['PatientGeometricalOffsetY'] = None
                else:
                    output_data0['PatientGeometricalOffsetX'] = None
                    output_data0['PatientGeometricalOffsetY'] = None

                if hasattr(dicom_header, 'HighDensityVoxelsSeries'):
                    output_data0['HighDensityVoxelsSeries'] = int(dicom_header.HighDensityVoxelsSeries)
                else:
                    output_data0['HighDensityVoxelsSeries'] = None

                if hasattr(dicom_header, 'HighDensityVoxelsSeriesPercent'):
                    output_data0['HighDensityVoxelsSeriesPercent'] = dicom_header.HighDensityVoxelsSeriesPercent
                else:
                    output_data0['HighDensityVoxelsSeriesPercent'] = None

                output_data0['Slices'] = len(dicom_headers)

                if hasattr(dicom_header, 'ImageOrientationPatient'):
                    output_data0['ImageOrientation'] = dicom_header.ImageOrientationPatient
                else:
                    output_data0['ImageOrientation'] = None

                cur.execute(
                    """INSERT INTO openssde_axial_list (SOPInstanceUID, StudyInstanceUID, SeriesInstanceUID,
                        MaskSuccess, ImageType, Manufacturer, Model, kVp, TableHeight,
                        PatientClipped, PatientGeometricalOffsetX, PatientGeometricalOffsetY,
                        HighDensityVoxelsSeries, HighDensityVoxelsSeriesPercent, Slices, ImageOrientation) VALUES
                        (%(SOPInstanceUID)s, %(StudyInstanceUID)s, %(SeriesInstanceUID)s,
                        %(MaskSuccess)s, %(ImageType)s, %(Manufacturer)s, %(Model)s,
                        %(kVp)s, %(TableHeight)s, %(PatientClipped)s,
                        %(PatientGeometricalOffsetX)s, %(PatientGeometricalOffsetY)s,
                        %(HighDensityVoxelsSeries)s, %(HighDensityVoxelsSeriesPercent)s,
                        %(Slices)s, %(ImageOrientation)s) RETURNING id;
                    """, output_data0)

                inserted_id = cur.fetchone()[0]

            # Go through export attributes to make sure they exist, and if they don't, insert default/error values
            output_data = dict()
            # Go through all output data and check if the attributes exist. If they don't, set the value in the dict to
            # None
            output_data['AxialListId'] = inserted_id

            if hasattr(dicom_header, 'AcquisitionNumber'):
                output_data['AcquisitionNumber'] = dicom_header.AcquisitionNumber
            else:
                output_data['AcquisitionNumber'] = None

            if hasattr(dicom_header, 'Exposure'):
                output_data['mAs'] = float(dicom_header.Exposure)
            else:
                output_data['mAs'] = None

            if hasattr(dicom_header, 'SliceThickness'):
                output_data['SliceThickness'] = float(dicom_header.SliceThickness)
            else:
                output_data['SliceThickness'] = None

            if hasattr(dicom_header, 'CTDIvol'):
                if dicom_header.CTDIvol != 'Not found':
                    output_data['CTDIvol'] = float(dicom_header.CTDIvol)
                else:
                    output_data['CTDIvol'] = None
            else:
                output_data['CTDIvol'] = None

            if hasattr(dicom_header, 'SliceLocation'):
                output_data['ZPosition'] = dicom_header.SliceLocation
            else:
                output_data['ZPosition'] = None

            if hasattr(dicom_header, 'MeanHU'):
                output_data['MeanHU'] = dicom_header.MeanHU
            else:
                output_data['MeanHU'] = None

            if hasattr(dicom_header, 'MedianHU'):
                output_data['MedianHU'] = dicom_header.MedianHU
            else:
                output_data['MedianHU'] = None

            if hasattr(dicom_header, 'WED_cm'):
                output_data['WED'] = dicom_header.WED_cm
                output_data['CF16WED'] = ed_to_cf_16cm(dicom_header.WED_cm)
                output_data['CF32WED'] = ed_to_cf_32cm(dicom_header.WED_cm)
            else:
                output_data['WED'] = None
                output_data['CF16WED'] = None
                output_data['CF32WED'] = None

            if hasattr(dicom_header, 'LAT_cm'):
                output_data['LAT'] = dicom_header.LAT_cm
                if dicom_header.LAT_cm is not None:
                    output_data['CF16LAT'] = lat_to_cf_16cm(dicom_header.LAT_cm)
                    output_data['CF32LAT'] = lat_to_cf_32cm(dicom_header.LAT_cm)
                else:
                    output_data['CF16LAT'] = None
                    output_data['CF32LAT'] = None
            else:
                output_data['LAT'] = None
                output_data['CF16LAT'] = None
                output_data['CF32LAT'] = None

            if hasattr(dicom_header, 'AP_cm'):
                output_data['AP'] = dicom_header.AP_cm
                if dicom_header.AP_cm is not None:
                    output_data['CF16AP'] = ap_to_cf_16cm(dicom_header.AP_cm)
                    output_data['CF32AP'] = ap_to_cf_32cm(dicom_header.AP_cm)
                    if hasattr(dicom_header, 'LAT_cm') and dicom_header.LAT_cm is not None:
                        output_data['CF16APLAT'] = lat_ap_to_cf_16cm(dicom_header.AP_cm + dicom_header.LAT_cm)
                        output_data['CF32APLAT'] = lat_ap_to_cf_32cm(dicom_header.AP_cm + dicom_header.LAT_cm)
                    else:
                        output_data['CF16APLAT'] = None
                        output_data['CF32APLAT'] = None
                else:
                    output_data['CF16AP'] = None
                    output_data['CF32AP'] = None
                    output_data['CF16APLAT'] = None
                    output_data['CF32APLAT'] = None
            else:
                output_data['AP'] = None
                output_data['CF16AP'] = None
                output_data['CF32AP'] = None
                output_data['CF16APLAT'] = None
                output_data['CF32APLAT'] = None

            if hasattr(dicom_header, 'EED_cm'):
                output_data['EED'] = dicom_header.EED_cm
                if dicom_header.EED_cm is not None:
                    output_data['CF16EED'] = ed_to_cf_16cm(dicom_header.EED_cm)
                    output_data['CF32EED'] = ed_to_cf_32cm(dicom_header.EED_cm)
                else:
                    output_data['CF16EED'] = None
                    output_data['CF32EED'] = None
            else:
                output_data['EED'] = None
                output_data['CF16EED'] = None
                output_data['CF32EED'] = None

            if hasattr(dicom_header, 'EquivalentAreaDiameter_cm'):
                output_data['EAD'] = dicom_header.EquivalentAreaDiameter_cm
            else:
                output_data['EAD'] = None

            if hasattr(dicom_header, 'SinogramEDcm'):
                output_data['SinogramED'] = dicom_header.SinogramEDcm
                if dicom_header.SinogramEDcm is not None:
                    output_data['CF16SinogramED'] = ed_to_cf_16cm(dicom_header.SinogramEDcm)
                    output_data['CF32SinogramED'] = ed_to_cf_32cm(dicom_header.SinogramEDcm)
                else:
                    output_data['SinogramED'] = None
                    output_data['CF16SinogramED'] = None
                    output_data['CF32SinogramED'] = None
            else:
                output_data['SinogramED'] = None
                output_data['CF16SinogramED'] = None
                output_data['CF32SinogramED'] = None

            if hasattr(dicom_header, 'SinogramLATcm'):
                output_data['SinogramLATcm'] = dicom_header.SinogramLATcm
                if dicom_header.SinogramLATcm is not None:
                    output_data['CF16SinogramLAT'] = ed_to_cf_16cm(dicom_header.SinogramLATcm)
                    output_data['CF32SinogramLAT'] = ed_to_cf_32cm(dicom_header.SinogramLATcm)
                else:
                    output_data['CF16SinogramLAT'] = None
                    output_data['CF32SinogramLAT'] = None
            else:
                output_data['SinogramLATcm'] = None
                output_data['CF16SinogramLAT'] = None
                output_data['CF32SinogramLAT'] = None

            if hasattr(dicom_header, 'SinogramLATDegree'):
                output_data['SinogramLATDegree'] = dicom_header.SinogramLATDegree
            else:
                output_data['SinogramLATDegree'] = None

            if hasattr(dicom_header, 'SinogramAPcm'):
                output_data['SinogramAPcm'] = dicom_header.SinogramAPcm
                if dicom_header.SinogramAPcm is not None:
                    output_data['CF16SinogramAP'] = ed_to_cf_16cm(dicom_header.SinogramAPcm)
                    output_data['CF32SinogramAP'] = ed_to_cf_32cm(dicom_header.SinogramAPcm)
                else:
                    output_data['CF16SinogramAP'] = None
                    output_data['CF32SinogramAP'] = None
            else:
                output_data['SinogramAPcm'] = None
                output_data['CF16SinogramAP'] = None
                output_data['CF32SinogramAP'] = None

            if hasattr(dicom_header, 'SinogramAPDegree'):
                output_data['SinogramAPDegree'] = dicom_header.SinogramAPDegree
            else:
                output_data['SinogramAPDegree'] = None

            if hasattr(dicom_header, 'StartTime'):
                output_data['SliceCalculationStartTime'] = dicom_header.StartTime
            else:
                output_data['SliceCalculationStartTime'] = None

            if hasattr(dicom_header, 'CalculationTime'):
                output_data['SliceCalculationTime'] = dicom_header.CalculationTime
            else:
                output_data['SliceCalculationTime'] = None

            cur.execute(
                """INSERT INTO openssde_axial_sizes (AcquisitionNumber, AxialListId, mAs, SliceThickness, CTDIvol,
                    ZPosition, MeanHU, MedianHU, WED, CF16WED, CF32WED, LAT, CF16LAT, CF32LAT, AP, CF16AP, CF32AP,
                    CF16APLAT, CF32APLAT, EED, CF16EED, CF32EED, EAD, SinogramED, CF16SinogramED, CF32SinogramED,
                    SinogramLATcm, SinogramLATDegree, CF16SinogramLAT, CF32SinogramLAT,
                    SinogramAPcm, SinogramAPDegree, CF16SinogramAP, CF32SinogramAP,
                    SliceCalculationStartTime, SliceCalculationTime) VALUES
                    (%(AcquisitionNumber)s, %(AxialListId)s, %(mAs)s, %(SliceThickness)s, %(CTDIvol)s,
                    %(ZPosition)s, %(MeanHU)s, %(MedianHU)s, %(WED)s, %(CF16WED)s, %(CF32WED)s,
                    %(LAT)s, %(CF16LAT)s, %(CF32LAT)s, %(AP)s, %(CF16AP)s, %(CF32AP)s, %(CF16APLAT)s, %(CF32APLAT)s,
                    %(EED)s, %(CF16EED)s, %(CF32EED)s, %(EAD)s, %(SinogramED)s, %(CF16SinogramED)s, %(CF32SinogramED)s,
                    %(SinogramLATcm)s, %(SinogramLATDegree)s, %(CF16SinogramLAT)s, %(CF32SinogramLAT)s,
                    %(SinogramAPcm)s, %(SinogramAPDegree)s, %(CF16SinogramAP)s, %(CF32SinogramAP)s,
                    %(SliceCalculationStartTime)s, %(SliceCalculationTime)s);
                """, output_data)
            # Commit the data to the database
            conn.commit()

            n += 1


def export_localizer_result(dicom_header, export2='database', output_file=None, database_connection_parameters=None):
    """ Export the SSDE calculation results for localizer images.

        :param dicom_header:
        :param export2:
        :param output_file:
        :param database_connection_parameters:
        :return:
        """
    if export2 == 'database':
        if database_connection_parameters['engine'] == "postgresql":
            import psycopg2
            conn = psycopg2.connect(database=database_connection_parameters['database'],
                                    user=database_connection_parameters['user'],
                                    password=database_connection_parameters['password'],
                                    host=database_connection_parameters['host'],
                                    port=database_connection_parameters['port'])
        else:
            raise ImportError('There is no packages implemented/installed for the specified database engine')

        cur = conn.cursor()

        output_data = dict()

        if hasattr(dicom_header, 'SOPInstanceUID'):
            output_data['SOPInstanceUID'] = dicom_header.SOPInstanceUID
        else:
            output_data['SOPInstanceUID'] = None

        if hasattr(dicom_header, 'StudyInstanceUID'):
            output_data['StudyInstanceUID'] = dicom_header.StudyInstanceUID
        else:
            output_data['StudyInstanceUID'] = None

        if hasattr(dicom_header, 'SeriesInstanceUID'):
            output_data['SeriesInstanceUID'] = dicom_header.SeriesInstanceUID
        else:
            output_data['SeriesInstanceUID'] = None

        if hasattr(dicom_header, 'AcquisitionNumber'):
            output_data['AcquisitionNumber'] = dicom_header.AcquisitionNumber
        else:
            output_data['AcquisitionNumber'] = None

        if hasattr(dicom_header, 'MaskSuccess'):
            output_data['MaskSuccess'] = dicom_header.MaskSuccess
        else:
            output_data['MaskSuccess'] = None

        if hasattr(dicom_header, 'ImageOrientationPatient'):
            x_row, x_col, y_row, y_col, z_row, z_col = dicom_header.ImageOrientationPatient
            if (1.0 - abs(x_col)) < 0.01:
                output_data['ViewType'] = 'Side view'
            else:
                output_data['ViewType'] = 'Front view'
        else:
            output_data['ViewType'] = None

        if hasattr(dicom_header, 'Manufacturer'):
            output_data['Manufacturer'] = dicom_header.Manufacturer
        else:
            output_data['Manufacturer'] = None

        if hasattr(dicom_header, 'ManufacturerModelName'):
            output_data['Model'] = dicom_header.ManufacturerModelName
        else:
            output_data['Model'] = None

        if hasattr(dicom_header, 'KVP'):
            output_data['kVp'] = float(dicom_header.KVP)
        else:
            output_data['kVp'] = None

        if hasattr(dicom_header, 'Exposure'):
            output_data['mAs'] = float(dicom_header.Exposure)
        else:
            output_data['mAs'] = None

        # Insert base data into localizer list table
        cur.execute(
            """INSERT INTO openssde_localizer_list (SOPInstanceUID, StudyInstanceUID, SeriesInstanceUID, AcquisitionNumber,
                MaskSuccess, ViewType, Manufacturer, Model, kVp, mAs) VALUES
                (%(SOPInstanceUID)s, %(StudyInstanceUID)s, %(SeriesInstanceUID)s, %(AcquisitionNumber)s,
                %(MaskSuccess)s, %(ViewType)s, %(Manufacturer)s, %(Model)s,
                %(kVp)s, %(mAs)s) RETURNING id;
            """, output_data)
        # Commit the data to the database
        inserted_id = cur.fetchone()[0]

        # Insert size results into the localizer size table
        for i in range(len(dicom_header.LocalizerResults[:, 0])):
            output_data = dict()
            output_data['LocalizerListId'] = inserted_id
            output_data['Pos'] = dicom_header.LocalizerResults[i, 0]
            output_data['Size_cm'] = dicom_header.LocalizerResults[i, 1]
            output_data['WED_cm'] = dicom_header.WEDcm

            if dicom_header.WEDcm is not None:
                output_data['CF16WED'] = ed_to_cf_16cm(dicom_header.WEDcm)
                output_data['CF32WED'] = ed_to_cf_32cm(dicom_header.WEDcm)
            else:
                output_data['CF16WED'] = None
                output_data['CF32WED'] = None

            if hasattr(dicom_header, 'MenkeResults'):
                output_data['Menke_WED_cm'] = dicom_header.MenkeResults[i, 1]
                if dicom_header.MenkeResults[i, 1] is not None:
                    output_data['CF16WEDMenke'] = ed_to_cf_16cm(dicom_header.MenkeResults[i, 1])
                    output_data['CF32WEDMenke'] = ed_to_cf_32cm(dicom_header.MenkeResults[i, 1])
                else:
                    output_data['CF16WEDMenke'] = None
                    output_data['CF32WEDMenke'] = None
            else:
                output_data['CF16WEDMenke'] = None
                output_data['CF32WEDMenke'] = None

            cur.execute(
                """INSERT INTO openssde_localizer_sizes (LocalizerListId, Pos, Size_cm,
                     WED_cm, CF16WED, CF32WED, Menke_WED_cm, CF16Menke, CF32Menke) VALUES
                    (%(LocalizerListId)s, %(Pos)s, %(Size_cm)s,
                     %(WED_cm)s, %(CF16WED)s, %(CF32WED)s,
                     %(Menke_WED_cm)s, %(CF16WEDMenke)s, %(CF32WEDMenke)s);
                """, output_data)
            # Commit the data to the database
            conn.commit()

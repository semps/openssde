"""
    MIT

    OpenSSDE - reads CT-dicom files to estimate patient size and extract information for choosing SSDE conversion factor
    Copyright (c) 2016, 2017 Josef Lundman & Morgan Nyberg

    This file is part of OpenSSDE.
"""
import os
import configparser as cp
import time
import sys
import matplotlib; matplotlib.use('Agg')
# import matplotlib.pyplot as plt2
import dicom
from dcm_import import find_all_dcm, import_dcm_images
from image_handling_tools import *
from export_ssde_results import *
import logging


class AutomaticSSDE:
    """ Class for building the OpenSSDE model at the import of files, calculate the SSDE and export the results

    """
    def __init__(self, file_path=None):
        self.file_list = []
        self.export2db = False
        self.export2file = False
        if file_path is not None:
            self.file_path = file_path
        self.image_info = dict()
        self.images_info = dict()
        self.images = ''
        self.db = dict()
        self.image_type = ''

        config = cp.ConfigParser()
        config.read(os.path.join(os.path.dirname(__file__), 'openssde.ini'))  # Read the configuration file
        # config.read(r'openssde.ini')

        self.logfile = os.path.join(config['Debug']['LogFolder'], (time.strftime('%Y-%m-%d') + '_OpenSSDE.log'))

        self.log = logging
        self.log.basicConfig(format='%(asctime)s\t%(levelname)s:\t%(message)s',
                             filename=self.logfile,
                             level=config['Debug']['Level'])
        self.log.info('STARTED OpenSSDE on file path:\t%s', file_path)
        # logging.basicConfig(format='%(asctime)-15s\t%(levelname)s: %(message)s',
        #                     filename=self.logfile,
        #                     level=config['Debug']['Level'])
        # logging.info('STARTED OpenSSDE')

        # Check which type of export is chosen
        if config['Export']['Type'] == 'database':
            self.export2db = True
            self.db['engine'] = config['DatabaseSettings']['engine']
            self.db['database'] = config['DatabaseSettings']['database']
            self.db['host'] = config['DatabaseSettings']['host']
            self.db['port'] = config['DatabaseSettings']['port']
            self.db['user'] = config['DatabaseSettings']['user']
            self.db['password'] = config['DatabaseSettings']['password']
        elif config['Export']['Type'] == 'file':
            self.export2file = True
            self.output_file = config['Export']['Location']

        self.RadonOrHough = 0
        if config['Methods']['RadonOrHough'] is not None:
            if config['Methods']['RadonOrHough'] == 'Hough':
                self.RadonOrHough = 1
            elif config['Methods']['RadonOrHough'] == 'Radon':
                self.RadonOrHough = 2

        # Gather the vendor specific segmentation calibration data
        self.VendorSpecificSegmentationCalibration = dict()
        for tmp in config['SegmentationCalibration']:
            tmp1 = config['SegmentationCalibration'][tmp]
            tmp1 = tmp1.split(', ')
            if ~hasattr(self.VendorSpecificSegmentationCalibration, tmp):
                self.VendorSpecificSegmentationCalibration[tmp] = dict()
            self.VendorSpecificSegmentationCalibration[tmp]['Vendor'] = tmp1[0]
            for j in range(1, (len(tmp1)-1), 2):
                self.VendorSpecificSegmentationCalibration[tmp][tmp1[j]] = tmp1[j+1]

        self.VendorSpecificLocalizerSettings = []  # This value will be set for each localizer image later

        if config['Debug']['Visual'] == 'False':
            self.visual = False
        else:
            self.visual = True
        self.report = config['Debug']['Report']
        if self.report:
            self.report_path = config['Debug']['ReportPath']
        self.calibrate = config['Debug']['Calibrate']
        self.verbose = config['Debug']['Verbose']
        self.dcm_info = dict()
        self.current_dcm_info = dict()
        self.localizer_file_name = ''
        self.slice_position_order = ''
        self.ordered_file_names = ''
        self.image_matrix = ''
        self.image_mask = ''
        self.patient_clipped = False
        self.mask_success = False
        self.edge_image_mask = ''
        self.current_dcm_info_order = []
        self.start_time = time.time()

    def list_images_info(self):
        """Look through the given folder for CT images.

        :return: A nested dictionary containing the images StudyInstanceUID, SeriesInstanceUID, z-position and path
        """
        if self.file_path is not None:
            self.log.debug('Listing files in "%s"', self.file_path)
            dcm_info = find_all_dcm(root=self.file_path, logfile=self.log)
            if 'CT' in dcm_info.keys():
                self.log.debug('Found %s DICOM files', len(dcm_info))
                self.dcm_info = dcm_info
            else:
                self.log.error('No DICOM CT files were found in the directory')
                raise IOError('No DICOM CT files were found under the supplied directory')
        else:
            self.log.error('No directory was given to search for files')
            raise IOError('No path to a image root directory for the file search was given')

    def import_and_calculate(self):
        """ Import the image matrix, segment the image to find the patient contour, calculate the size of the patient,
        determine the SSDE conversion factor for each slice, export the result

        :return:
        """
        if self.dcm_info is not None and 'CT' in self.dcm_info.keys():
            self.log.debug('Importing files')
            for i in self.dcm_info['CT'].keys():
                for j in self.dcm_info['CT'][i].keys():
                    for k in self.dcm_info['CT'][i][j].keys():
                        self.start_time = time.time()
                        self.image_type = k
                        if k == 'AXIAL' or k == 'REFORMATTED':
                            slice_positions = [float(l) for l in self.dcm_info['CT'][i][j][k].keys()]
                            self.slice_position_order = np.argsort(np.array(slice_positions))
                            self.ordered_file_names = [self.dcm_info['CT'][i][j][k][slice_positions[l]]['file_path']
                                                       for l in self.slice_position_order]
                            try:
                                self.log.debug('Importing axial images from %s',
                                               os.path.dirname(os.path.abspath(self.ordered_file_names[0])))
                                self.image_matrix = import_dcm_images(self.ordered_file_names, k,
                                                                      [self.dcm_info['CT'][i][j][k][slice_positions[0]][
                                                                           'dcm_header'][0x28, 0x10].value,
                                                                       self.dcm_info['CT'][i][j][k][slice_positions[0]][
                                                                           'dcm_header'][0x28, 0x11].value
                                                                       ])
                                self.current_dcm_info = [self.dcm_info['CT'][i][j][k][slice_positions[l]][
                                                             'dcm_header'] for l in self.slice_position_order]
                            except:
                                print('Could not import the image matrix or set the current dcm info')
                                pass

                            try:
                                self.mask_success, self.image_mask, self.patient_clipped, self.edge_image_mask =\
                                    segment_axial(self.image_matrix, self.visual)

                                if self.mask_success:
                                    try:
                                        central_slice = int(np.ceil(np.divide(float(self.image_mask.shape[2]), 2.0))) - 1
                                        patient_offset = calculate_patient_geometrical_offset(
                                            mask_slice=self.image_mask[:, :, central_slice],
                                            dicom_header=self.current_dcm_info[central_slice]
                                        )
                                        if self.report:
                                            self.create_report('AXIAL')
                                    except:
                                        self.log.debug('Could not calculated patient centering offset')
                                        patient_offset = None
                                else:
                                    patient_offset = None
                                # Loop through dicom headers and add info about whether patient is clipped or not
                                for di in self.current_dcm_info:
                                    di.PatientClipped = self.patient_clipped
                                    di.StartTime = self.start_time
                                    di.PatientOffset = patient_offset
                            except:
                                print('Could not segment the the body contour from the image matrix')
                                pass
                            self.calculate_ssde()

                        elif k == 'LOCALIZER':
                            images = [i for i in self.dcm_info['CT'][i][j][k].keys()]
                            file_names = [self.dcm_info['CT'][i][j][k][images[l]]['file_path'] for l in
                                          range(len(images))]
                            dcm_headers = [self.dcm_info['CT'][i][j][k][images[l]]['dcm_header'] for l in
                                           range(len(images))]
                            matrix_sizes = [[self.dcm_info['CT'][i][j][k][images[l]]['dcm_header'][0x28, 0x10].value,
                                             self.dcm_info['CT'][i][j][k][images[l]]['dcm_header'][0x28, 0x11].value]
                                            for l in range(len(images))]
                            self.images = dict()
                            # vendors = [(self.VendorSpecificSegmentationCalibration[ven_key]['Vendor'], ven_key)
                            #            for ven_key in self.VendorSpecificSegmentationCalibration.keys()]
                            test = []
                            for l in range(len(images)):
                                tmp = [self.VendorSpecificSegmentationCalibration[ven_key] for ven_key in
                                       self.VendorSpecificSegmentationCalibration.keys() if
                                       self.VendorSpecificSegmentationCalibration[ven_key]['Vendor'] ==
                                       self.dcm_info['CT'][i][j][k][images[l]]['dcm_header'].Manufacturer]
                                if len(tmp) > 0:
                                    test.append(tmp)
                                else:
                                    test.append([dict()])

                            for l in range(len(file_names)):
                                self.localizer_file_name = file_names[l]
                                if self.verbose:
                                    print(file_names[l])
                                if 'LocalizerWindowRangeMin' in test[l][0].keys() \
                                        and 'LocalizerWindowRangeMax' in test[l][0].keys():
                                    window_range = range(int(test[l][0]['LocalizerWindowRangeMin']),
                                                         int(test[l][0]['LocalizerWindowRangeMax']))
                                else:
                                    window_range = range(-1000, 100)

                                self.VendorSpecificLocalizerSettings = test[l]

                                self.current_dcm_info = dcm_headers[l]
                                self.log.debug('Importing localizer images from %s',
                                               os.path.dirname(os.path.abspath(file_names[l])))
                                self.images = import_dcm_images([file_names[l]], k, matrix_sizes[l])
                                self.mask_success, self.image_mask, self.edge_image_mask =\
                                    segment_localizer(self.images, window_range)

                                if self.mask_success and self.report:
                                    self.create_report('LOCALIZER')

                                # Calculate the SSDE and export results
                                self.calculate_ssde()
        else:
            raise IOError('No images to import were found')

    def calculate_ssde(self):
        """ Perform patient size calculations, calculate the conversion factors and patient offset. After the
        calculations are done, the results are exported. Create a report if report mode is set in openssde.ini.

        :return:
        """
        # Select calculation and exporting depending on image type
        if self.image_type == 'AXIAL' or self.image_type == 'REFORMATTED':
            self.log.debug('Calculating SSDE for axial images')
            self.current_dcm_info = calculate_equivalent_diameter(image_matrix=self.image_matrix,
                                                                  image_mask=self.image_mask,
                                                                  dicom_headers=self.current_dcm_info,
                                                                  perform_hough_or_radon_transform=self.RadonOrHough)

            # Set the calculation time as the mean time per slice for the calculations done
            calculation_time = (time.time() - self.start_time) / len(self.current_dcm_info)
            for di in self.current_dcm_info:
                di.CalculationTime = calculation_time

            if self.export2db:
                export_location = 'database'
            elif self.export2file:
                export_location = 'file'
            else:
                print('Incorrect choice of export location made')
                return None
            try:
                self.log.debug('Exporting OpenSSDE results for axial images')
                export_axial_result(dicom_headers=self.current_dcm_info,
                                    export2=export_location,
                                    database_connection_parameters=self.db,
                                    mask_success=self.mask_success)
            except:
                self.log.error('Could not export the result of the axial slices calculations')
                print('Could not export the result of the calculation')
                return None

            return True
        elif self.image_type == 'LOCALIZER':
            if len(self.VendorSpecificLocalizerSettings) > 0:
                vendor_settings = self.VendorSpecificLocalizerSettings
            else:
                vendor_settings = None

            self.log.debug('Calculating SSDE for localizer')
            self.current_dcm_info = estimate_scout(image_matrix=self.images,
                                                   image_mask=self.image_mask,
                                                   dicom_header=self.current_dcm_info,
                                                   manufacturer_specific_settings=vendor_settings,
                                                   visual=False,
                                                   verbose=False)

            if self.export2db:
                export_location = 'database'
            elif self.export2file:
                export_location = 'file'
            else:
                self.log.error('Invalid export location')
                print('Incorrect choice of export location made')
                return None
            try:
                self.log.debug('Exporting OpenSSDE result for localizer')
                self.current_dcm_info.MaskSuccess = self.mask_success
                export_localizer_result(dicom_header=self.current_dcm_info,
                                        export2=export_location,
                                        database_connection_parameters=self.db)
            except:
                self.log.error('Could not export the result of the localizer calculations')
                print('Could not export the result of the calculation')
                return None

            return True

    def create_report(self, image_type):
        """ Create a set of images showing the result of the segmentations. Save them to the specified report parth.

        :param image_type: a string. 'AXIAL' or 'LOCALIZER'
        :return:
        """
        if image_type == 'AXIAL':
            mask_output = os.path.join(self.report_path,
                                       self.current_dcm_info[0].StudyInstanceUID,
                                       self.current_dcm_info[0].SeriesInstanceUID, ('series' + str(self.current_dcm_info[0].SeriesNumber)))
            if not os.path.exists(mask_output):
                os.makedirs(mask_output)
            self.log.debug('Saving axial image masks to "%s"', mask_output)
            for i in range(len(self.slice_position_order)):
                dcmout = dicom.read_file(self.ordered_file_names[i])
                dcmout.PixelData = self.image_mask[:, :, i].astype('int16').tostring()
                dcmout.RescaleIntercept = '0'
                dcmout.RescaleSlope = '1'
                dcmout.WindowCenter = '0'
                dcmout.WindowWidth = '1'
                dcmout.save_as(os.path.join(mask_output, (str(i) + '.dcm')))

        else:
            mask_output = os.path.join(self.report_path, self.current_dcm_info.StudyInstanceUID,
                                       self.current_dcm_info.SeriesInstanceUID,
                                       ('series' + str(self.current_dcm_info.SeriesNumber)))

            if not os.path.exists(mask_output):
                os.makedirs(mask_output)

            self.log.debug('Saving localizer mask to "%s"', mask_output)
            dcmout = dicom.read_file(self.localizer_file_name)
            dcmout.PixelData = self.image_mask[:, :].astype('int16').tostring()
            dcmout.RescaleIntercept = '0'
            dcmout.RescaleSlope = '1'
            dcmout.WindowCenter = '0'
            dcmout.WindowWidth = '1'
            dcmout.save_as(os.path.join(mask_output, (str(self.current_dcm_info.AcquisitionNumber) + '.dcm')))

        self.log.debug('Mask/-s saved')

    def calibrate_segmentation(self):
        """ Run the segmentation in calibration mode. Present the result to the user and let the user input the correct
        calibration for the specific machine of the images. Print the resulting calibration in the openssde.ini-file
        """


# Loop through the arguments (paths) and try to calculate the patient size, offset and SSDE for them
for arg in sys.argv[1:]:
    # print(arg)
    filepath = os.path.abspath(arg)
    if os.path.isfile(filepath):
        filepath = os.path.abspath(os.path.join(filepath, os.pardir))
    # print(filepath)
    if os.path.exists(filepath):
        ssde = AutomaticSSDE(filepath)
        ssde.list_images_info()
        ssde.import_and_calculate()
        ssde.log.info('FINISHED OpenSSDE')
